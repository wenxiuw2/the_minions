### MeanPaper (dl35) 11/12/2023
- Created a new Socket for weapon carrying (modified Quinn)
- Created a new action for the ThirdPerson Character (under folder: ThirdPerson/Input/Actions):
  - Press "E" to interact with the weapon
- Created a weapon map in ThirdPerson BP (the one that carries a AR4 rifle)
  - Modified the map to let the character carry more weapons, only supporting carrying Rifle on the back currently 
  - To support more weapons on the character, modified BP_Weapon_Master to add more component to the character (attach actor to component), the problem right: attached weapon does not show on the character, someone please check this

### MeanPaper (dl35) 11/13/2023
- weapon attachment problem is resolved. The problem is caused by character skeleton socket. The weapon that character picks up need to match the weapon attached to the socket
- some melee weapons are missing physics
- basic weapon swapping is done

### MeanPaper (dl35) 11/18/2023
- Weapon switching is ready, waiting for more montage, for more information please look at the third person shooter BP
- Basic damage to enemy is ready:
  - Look at the apply damage function in third person shooter BP
  - Look at the damage event in the pursuer BP, the actor that is hit is call pursuer (it is a BP)
  - The 'hit' mechanism: Pursuer capsule's collision preset is set to blockAllDynamics so that the hit event can be generated and detected
  - Add max ammo for weapone magazines and the base damage for each weapon
- TASK:
  - The Character NEED to pick up a gun, rather than attach to a gun (please work on that my dear teammate)
- Pending task???
  - A master blueprint for enemy and a look up table for it
  - We can do enemy hit check: head, body, arm, thigh
  